## [1.0.16](https://gitlab.com/lmco/robots/semantic-release-image/compare/v1.0.15...v1.0.16) (2025-02-17)

### Bug Fixes

* **deps:** update dependency semantic-release-fail-on-major-bump to v1.0.2 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([1ed9b4d](https://gitlab.com/lmco/robots/semantic-release-image/commit/1ed9b4de8734a10f22b460ff96809eaaa0a1328d))

## [1.0.15](https://gitlab.com/lmco/robots/semantic-release-image/compare/v1.0.14...v1.0.15) (2025-02-16)

### Bug Fixes

* **deps:** update dependency semantic-release to v24.2.3 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([e520982](https://gitlab.com/lmco/robots/semantic-release-image/commit/e52098233151e91a85efbb5e0a453bd6ed73ce1c))

## [1.0.14](https://gitlab.com/lmco/robots/semantic-release-image/compare/v1.0.13...v1.0.14) (2025-02-12)

### Bug Fixes

* **deps:** update dependency semantic-release to v24.2.2 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([b84cf79](https://gitlab.com/lmco/robots/semantic-release-image/commit/b84cf791bdf8d7c9ea9b93760fade52bcb1c017c))

## [1.0.13](https://gitlab.com/lmco/robots/semantic-release-image/compare/v1.0.12...v1.0.13) (2025-02-06)

### Bug Fixes

* **deps:** update dependency @semantic-release/exec to v7.0.3 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([02c8459](https://gitlab.com/lmco/robots/semantic-release-image/commit/02c8459f88ab757c9655a4f7a1704a80e95bb962))

## [1.0.12](https://gitlab.com/lmco/robots/semantic-release-image/compare/v1.0.11...v1.0.12) (2025-02-02)

### Bug Fixes

* **deps:** update dependency @semantic-release/exec to v7.0.2 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([1d09136](https://gitlab.com/lmco/robots/semantic-release-image/commit/1d09136f7277d36745859caf943c877c43c72cdb))

## [1.0.11](https://gitlab.com/lmco/robots/semantic-release-image/compare/v1.0.10...v1.0.11) (2025-02-01)

### Bug Fixes

* **deps:** update dependency @semantic-release/exec to v7 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([6f64ec7](https://gitlab.com/lmco/robots/semantic-release-image/commit/6f64ec78fec2b67223cc761d7460364b5147735e))

## [1.0.10](https://gitlab.com/lmco/robots/semantic-release-image/compare/v1.0.9...v1.0.10) (2025-01-16)

### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v13.2.4 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([3f26b56](https://gitlab.com/lmco/robots/semantic-release-image/commit/3f26b561bf1e3d0b632a320d20e5b3dd299ac188))
* **deps:** update dependency @semantic-release/release-notes-generator to v14.0.2 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([4a1f3b8](https://gitlab.com/lmco/robots/semantic-release-image/commit/4a1f3b88fad3fb8d8824672f87c111c49e4d69c5))
* **deps:** update semantic-release monorepo signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([f697521](https://gitlab.com/lmco/robots/semantic-release-image/commit/f697521a52860d4e391bf824ff8ef65dda0caabc))

## [1.0.9](https://gitlab.com/lmco/robots/semantic-release-image/compare/v1.0.8...v1.0.9) (2024-12-05)

### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v13.2.3 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([bbf3f6c](https://gitlab.com/lmco/robots/semantic-release-image/commit/bbf3f6ccb49a2a11e7b1623ef137b3a3fffcfa3c))

## [1.0.8](https://gitlab.com/lmco/robots/semantic-release-image/compare/v1.0.7...v1.0.8) (2024-12-04)

### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v13.2.2 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([fdf8e83](https://gitlab.com/lmco/robots/semantic-release-image/commit/fdf8e837cf2231d9bb73298a20a2f40dbc05d537))

## [1.0.7](https://gitlab.com/lmco/robots/semantic-release-image/compare/v1.0.6...v1.0.7) (2024-10-26)

### Bug Fixes

* **deps:** update dependency semantic-release to v24.1.3 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([28fe2ca](https://gitlab.com/lmco/robots/semantic-release-image/commit/28fe2ca32deb3be0e1c67b3800aff88a11ba6670))
* **deps:** update dependency semantic-release to v24.2.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([299a98d](https://gitlab.com/lmco/robots/semantic-release-image/commit/299a98dabef27530e79b6b51606f39b41a701785))

## [1.0.6](https://gitlab.com/lmco/robots/semantic-release-image/compare/v1.0.5...v1.0.6) (2024-09-27)

### Bug Fixes

* **deps:** update dependency semantic-release to v24.1.2 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([c883ebb](https://gitlab.com/lmco/robots/semantic-release-image/commit/c883ebbf0fcb1b78da8e9eff711435f5a3f0da74))

## [1.0.5](https://gitlab.com/lmco/robots/semantic-release-image/compare/v1.0.4...v1.0.5) (2024-09-17)

### Bug Fixes

* bump alpine image ([c8f39a0](https://gitlab.com/lmco/robots/semantic-release-image/commit/c8f39a0ed2a7c65be886031e835066b2deba69d6))

## [1.0.4](https://gitlab.com/lmco/robots/semantic-release-image/compare/v1.0.3...v1.0.4) (2024-09-11)

### Bug Fixes

* **deps:** update dependency semantic-release to v24.1.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([3cf4e70](https://gitlab.com/lmco/robots/semantic-release-image/commit/3cf4e704918e333718314aae3b33168503a022d6))

## [1.0.3](https://gitlab.com/lmco/robots/semantic-release-image/compare/v1.0.2...v1.0.3) (2024-09-09)

### Bug Fixes

* **deps:** update dependency ansi-regex to v6.1.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([3b8a179](https://gitlab.com/lmco/robots/semantic-release-image/commit/3b8a1796b183a92e902ed7cafc187f8592cb4ea5))

## [1.0.2](https://gitlab.com/lmco/robots/semantic-release-image/compare/v1.0.1...v1.0.2) (2024-08-17)

### Bug Fixes

* **deps:** update dependency semantic-release to v24.1.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([1a4f16f](https://gitlab.com/lmco/robots/semantic-release-image/commit/1a4f16f272aad8bc45aee1a1bd71a6ae383471af))

## [1.0.1](https://gitlab.com/lmco/robots/semantic-release-image/compare/v1.0.0...v1.0.1) (2024-07-31)

### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v13.2.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([734a87e](https://gitlab.com/lmco/robots/semantic-release-image/commit/734a87e106ac862ee295082b2409cd1f3a7e9fb3))

## [1.0.0](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.101...v1.0.0) (2024-07-09)

### Bug Fixes

* Correct signing ([d55575b](https://gitlab.com/lmco/robots/semantic-release-image/commit/d55575b070f3b065ee6988826718124274feb33f))
* **deps:** update dependency @semantic-release/gitlab to v13.0.4 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([66f0bc6](https://gitlab.com/lmco/robots/semantic-release-image/commit/66f0bc6dc671831ef0da401b46987361e4c8138b))
* **deps:** update dependency @semantic-release/gitlab to v13.1.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([8698e9a](https://gitlab.com/lmco/robots/semantic-release-image/commit/8698e9a2300f5765d024d7a56e6b69c937316dd2))
* **deps:** update dependency @semantic-release/gitlab to v13.2.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([f9245b2](https://gitlab.com/lmco/robots/semantic-release-image/commit/f9245b28e01a28f2ad6361f85f257db44790fc2b))
* **deps:** update dependency @semantic-release/npm to v12.0.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([8ea39e4](https://gitlab.com/lmco/robots/semantic-release-image/commit/8ea39e4f242095a27035cfc0377ae99c0b7ae008))
* **deps:** update dependency @semantic-release/release-notes-generator to v14.0.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([13985ac](https://gitlab.com/lmco/robots/semantic-release-image/commit/13985ac1e24fa12c1b3ad8391bdea333c05f9fcc))
* **deps:** update dependency conventional-changelog-conventionalcommits to v8 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([bbe735a](https://gitlab.com/lmco/robots/semantic-release-image/commit/bbe735a9f1094b92dfea7b749f7041a4a8c9172a))
* **deps:** update dependency semantic-release to v23.1.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([80521e6](https://gitlab.com/lmco/robots/semantic-release-image/commit/80521e6078824d2a8d740a4d3a75cec11fb180ef))
* **deps:** update dependency semantic-release to v23.1.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([178b6e8](https://gitlab.com/lmco/robots/semantic-release-image/commit/178b6e88595a0211283ca7747b170d13760b02d3))
* **deps:** update semantic-release monorepo signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([d6270c6](https://gitlab.com/lmco/robots/semantic-release-image/commit/d6270c62198016282ffc5ab00dd03fc98fc4bdde))
* Update the renovate json config ([a110fbf](https://gitlab.com/lmco/robots/semantic-release-image/commit/a110fbf8a0f9a45aeaed8a1eeaf56767afb2d57f))
* Update trivy ([6230c4f](https://gitlab.com/lmco/robots/semantic-release-image/commit/6230c4f3b2e529cfbf584e9a7076d92dc4e504b4))

## [0.0.101](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.100...v0.0.101) (2024-04-10)


### Bug Fixes

* Bump node version ([c383c34](https://gitlab.com/lmco/robots/semantic-release-image/commit/c383c34ff27ebb4f5210decae570a0740b6a9302))
* Install core poetry in base ([0a7d0cb](https://gitlab.com/lmco/robots/semantic-release-image/commit/0a7d0cbcb7941e9f2c0ba6670cfd2b4778e63dea))

## [0.0.100](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.99...v0.0.100) (2024-4-9)


### Bug Fixes

* **deps:** update dependency semantic-release to v23.0.8 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([ae693c3](https://gitlab.com/lmco/robots/semantic-release-image/commit/ae693c39a7b53ef22b1775c756fe392ec154c3c9))

## [0.0.99](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.98...v0.0.99) (2024-4-3)


### Bug Fixes

* **deps:** update dependency semantic-release to v23.0.7 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([662c828](https://gitlab.com/lmco/robots/semantic-release-image/commit/662c828720fb73851b546cf37277270a25c5b1a1))

## [0.0.98](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.97...v0.0.98) (2024-3-27)


### Bug Fixes

* **deps:** update semantic-release monorepo signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([2dfab08](https://gitlab.com/lmco/robots/semantic-release-image/commit/2dfab08db2bfefb3c1c1c9ebcb761293bf53d65e))

## [0.0.97](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.96...v0.0.97) (2024-3-27)


### Bug Fixes

* **deps:** update semantic-release monorepo signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([0e973d1](https://gitlab.com/lmco/robots/semantic-release-image/commit/0e973d1ace7fca8df93a3c58b708a54c7992f1c9))

## [0.0.96](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.95...v0.0.96) (2024-3-27)


### Bug Fixes

* Correct cosign version and automate ([6225320](https://gitlab.com/lmco/robots/semantic-release-image/commit/6225320723eb17dd9c66ccfc679808a99261aa29))
* Correct semantic-release cosign version ([6fc0963](https://gitlab.com/lmco/robots/semantic-release-image/commit/6fc0963d6b451c73c58221f9b142bba8a0945431))
* Test ignore installed ([7e467ff](https://gitlab.com/lmco/robots/semantic-release-image/commit/7e467ff54a61a68e46c2ce265f23add7b7ad8d0d))

## [0.0.95](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.94...v0.0.95) (2024-2-20)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v13.0.3 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([746dfff](https://gitlab.com/lmco/robots/semantic-release-image/commit/746dfff3358c0cb352d410ee94f97806e6fa02ca))

## [0.0.94](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.93...v0.0.94) (2024-2-7)


### Bug Fixes

* **deps:** update dependency semantic-release to v23.0.2 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([d75fb81](https://gitlab.com/lmco/robots/semantic-release-image/commit/d75fb818acbfffa39ed271d88c4bcb65e2e52df9))

## [0.0.93](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.92...v0.0.93) (2024-2-6)


### Bug Fixes

* **deps:** update dependency semantic-release to v23.0.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([fb2e728](https://gitlab.com/lmco/robots/semantic-release-image/commit/fb2e728aa2f8c1781bc3d920fbbece1dec5e2d59))

## [0.0.92](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.91...v0.0.92) (2024-1-12)


### Bug Fixes

* **deps:** update dependency semantic-release to v23 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([af390e3](https://gitlab.com/lmco/robots/semantic-release-image/commit/af390e383e85f4728ce729f05fcbedddd9348112))

## [0.0.91](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.90...v0.0.91) (2024-1-8)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v13.0.2 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([ea5d516](https://gitlab.com/lmco/robots/semantic-release-image/commit/ea5d5169a1320894da6f93bed3cc0e78fd364e45))

## [0.0.90](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.89...v0.0.90) (2023-12-21)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v13 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([c928a85](https://gitlab.com/lmco/robots/semantic-release-image/commit/c928a85fbcbfb78585af55457f0ffd3819afbfaa))

## [0.0.89](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.88...v0.0.89) (2023-12-12)


### Bug Fixes

* **deps:** update dependency semantic-release to v22.0.12 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([92360f8](https://gitlab.com/lmco/robots/semantic-release-image/commit/92360f878d5cf6cf8ac8757269f2eec4cc3d0e78))

## [0.0.88](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.87...v0.0.88) (2023-12-12)


### Bug Fixes

* **deps:** update dependency semantic-release to v22.0.11 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([24ca05f](https://gitlab.com/lmco/robots/semantic-release-image/commit/24ca05f9a88381fb73762ab32552181115f33f89))

## [0.0.87](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.86...v0.0.87) (2023-12-7)


### Bug Fixes

* **deps:** update dependency @semantic-release/npm to v11.0.2 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([be9aa7f](https://gitlab.com/lmco/robots/semantic-release-image/commit/be9aa7fff59b11a7e92def4c8f2613c286aaba0e))

## [0.0.86](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.85...v0.0.86) (2023-12-6)


### Bug Fixes

* **deps:** update dependency semantic-release to v22.0.10 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([305b790](https://gitlab.com/lmco/robots/semantic-release-image/commit/305b790af63cbc25e1866d3477b754146fe3dbf6))

## [0.0.85](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.84...v0.0.85) (2023-12-5)


### Bug Fixes

* **deps:** update dependency semantic-release to v22.0.9 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([000601f](https://gitlab.com/lmco/robots/semantic-release-image/commit/000601fa0d097dba365efeba4bb23512c006b631))

## [0.0.84](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.83...v0.0.84) (2023-11-27)


### Bug Fixes

* **deps:** update dependency @saithodev/semantic-release-backmerge to v4.0.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([58a70f3](https://gitlab.com/lmco/robots/semantic-release-image/commit/58a70f3844bb8f16f5d324cb09bec8b6f6b41e2e))

## [0.0.83](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.82...v0.0.83) (2023-11-27)


### Bug Fixes

* **deps:** update dependency @saithodev/semantic-release-backmerge to v4 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([50cfa74](https://gitlab.com/lmco/robots/semantic-release-image/commit/50cfa7481a44ee8b3cf8339e3b20bdc53f352525))

## [0.0.82](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.81...v0.0.82) (2023-11-17)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v12.1.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([da78d24](https://gitlab.com/lmco/robots/semantic-release-image/commit/da78d2466545ff3d49fa09c325c488ae25b31b04))

## [0.0.81](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.80...v0.0.81) (2023-11-17)


### Bug Fixes

* **deps:** update dependency semantic-release to v22.0.8 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([eba26b4](https://gitlab.com/lmco/robots/semantic-release-image/commit/eba26b4595411bd47596741c6c65b076eaba3c67))

## [0.0.80](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.79...v0.0.80) (2023-11-15)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v12.1.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([c43ac86](https://gitlab.com/lmco/robots/semantic-release-image/commit/c43ac865b1eb01404c905e92cda55d055ed7c896))

## [0.0.79](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.78...v0.0.79) (2023-11-06)


### Bug Fixes

* **deps:** update semantic-release monorepo signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([aca6ad0](https://gitlab.com/lmco/robots/semantic-release-image/commit/aca6ad0f6bcd2171fe8e4e77aeb3bb8846b9058e))

## [0.0.78](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.77...v0.0.78) (2023-11-04)


### Bug Fixes

* **deps:** update dependency @semantic-release/release-notes-generator to v12.0.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([7b53e96](https://gitlab.com/lmco/robots/semantic-release-image/commit/7b53e967d59b5f05082273635b9a86e48e1c7345))

## [0.0.77](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.76...v0.0.77) (2023-11-03)


### Bug Fixes

* **deps:** update dependency semantic-release to v22.0.7 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([a45c953](https://gitlab.com/lmco/robots/semantic-release-image/commit/a45c953d32d25e28e023161d11d8ef64107d40f7))

## [0.0.76](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.75...v0.0.76) (2023-11-02)


### Bug Fixes

* **deps:** update dependency @semantic-release/npm to v11.0.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([75eb025](https://gitlab.com/lmco/robots/semantic-release-image/commit/75eb025fbdc319cdc2646a0876ae68acba6d4dc8))

## [0.0.75](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.74...v0.0.75) (2023-10-31)


### Bug Fixes

* **deps:** update dependency semantic-release to v22.0.6 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([60932b6](https://gitlab.com/lmco/robots/semantic-release-image/commit/60932b62b1a165d90af5a2fca0a9129613f1086b))

## [0.0.74](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.73...v0.0.74) (2023-10-29)


### Bug Fixes

* **deps:** update dependency @semantic-release-plus/docker to v3.1.3 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([05236ea](https://gitlab.com/lmco/robots/semantic-release-image/commit/05236ea7cd77052a367c5915e60ddaafb5b51e2d))

## [0.0.73](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.72...v0.0.73) (2023-09-26)


### Bug Fixes

* Add helm-push and helm ([1f55686](https://gitlab.com/lmco/robots/semantic-release-image/commit/1f556869e3703cbc1d5f66c087a6213c43bec885))

## [0.0.72](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.71...v0.0.72) (2023-09-24)


### Bug Fixes

* **deps:** update dependency semantic-release to v22.0.5 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([9139ffd](https://gitlab.com/lmco/robots/semantic-release-image/commit/9139ffd44fa83adfc9d1239b936d4e183f092036))

## [0.0.71](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.70...v0.0.71) (2023-09-23)


### Bug Fixes

* **deps:** update semantic-release monorepo signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([85c15d9](https://gitlab.com/lmco/robots/semantic-release-image/commit/85c15d9c9b5ace78d071331c1e09b8d4d205eb19))

## [0.0.70](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.69...v0.0.70) (2023-09-23)


### Bug Fixes

* **deps:** update dependency @saithodev/semantic-release-backmerge to v3.2.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([f4fcb9f](https://gitlab.com/lmco/robots/semantic-release-image/commit/f4fcb9ff4fbf9b5d52473a9917e7dee3d9324875))

## [0.0.69](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.68...v0.0.69) (2023-09-23)


### Bug Fixes

* **deps:** update dependency semantic-release to v22.0.4 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([10a2370](https://gitlab.com/lmco/robots/semantic-release-image/commit/10a2370c20408fbbe9b8d119b37c0e2069d79eff))

## [0.0.68](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.67...v0.0.68) (2023-09-22)


### Bug Fixes

* **deps:** update dependency semantic-release to v22.0.2 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([247c173](https://gitlab.com/lmco/robots/semantic-release-image/commit/247c173689af0f87481bda5263da4d9c9f59bf4a))

## [0.0.67](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.66...v0.0.67) (2023-09-22)


### Bug Fixes

* **deps:** update dependency semantic-release-helm3 to v2.9.3 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([7b8924a](https://gitlab.com/lmco/robots/semantic-release-image/commit/7b8924a72062d5d56d2b5bf8e4fa809d823e0ee4))

## [0.0.66](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.65...v0.0.66) (2023-09-21)


### Bug Fixes

* **deps:** update dependency semantic-release to v22.0.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([7578256](https://gitlab.com/lmco/robots/semantic-release-image/commit/75782564e7dcf9924d3ee9aea18f23517422531d))

## [0.0.65](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.64...v0.0.65) (2023-09-20)


### Bug Fixes

* **deps:** update dependency @semantic-release/release-notes-generator to v11.0.7 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([6711290](https://gitlab.com/lmco/robots/semantic-release-image/commit/6711290d53a0eada6225af19361b2bcf1a2c1485))

## [0.0.64](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.63...v0.0.64) (2023-09-20)


### Bug Fixes

* Add helm, helm push ([ca981cb](https://gitlab.com/lmco/robots/semantic-release-image/commit/ca981cb648fd6b36fcb01dd1ab427f6b222bf34c))
* Add openssl ([a46cf7e](https://gitlab.com/lmco/robots/semantic-release-image/commit/a46cf7e80ecea168dbcef38cf1740513092b559a))
* **deps:** update dependency conventional-changelog-conventionalcommits to v7 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([ae83caa](https://gitlab.com/lmco/robots/semantic-release-image/commit/ae83caa0d9892504be81dc783e7d6d876119dec6))
* **deps:** update dependency conventional-changelog-conventionalcommits to v7.0.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([8454795](https://gitlab.com/lmco/robots/semantic-release-image/commit/8454795da069e54c02c191f1bb1ea31bc16cca45))
* **deps:** update dependency conventional-changelog-conventionalcommits to v7.0.2 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([5c1dad5](https://gitlab.com/lmco/robots/semantic-release-image/commit/5c1dad5e9ea298afef23435aaff1917ec2061600))
* **deps:** update dependency semantic-release-helm3 to v2.9.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([3c793f3](https://gitlab.com/lmco/robots/semantic-release-image/commit/3c793f3e0cd974fcb1e0269ecd5cf6a53b666481))
* **deps:** update dependency semantic-release-helm3 to v2.9.2 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([193268f](https://gitlab.com/lmco/robots/semantic-release-image/commit/193268fd3e85ba6145d1cd38c4589a971567fdf1))
* **deps:** update semantic-release monorepo signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([5079839](https://gitlab.com/lmco/robots/semantic-release-image/commit/5079839b2b77340a145883daedf2baedd1bd20f3))
* **deps:** update semantic-release monorepo signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([41f3347](https://gitlab.com/lmco/robots/semantic-release-image/commit/41f334791c72a4160002ae66a3539bd71e80ee06))
* **deps:** update semantic-release monorepo signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([b790bf4](https://gitlab.com/lmco/robots/semantic-release-image/commit/b790bf4fb887745e122c62980b18d034a5e50de7))
* Push podman images ([335d67c](https://gitlab.com/lmco/robots/semantic-release-image/commit/335d67cbf23b477a3f5215511c56c756e3116991))
* Start debugging ([92d8d6e](https://gitlab.com/lmco/robots/semantic-release-image/commit/92d8d6eacfa72569392238c87f77166992e4bcb8))
* Update node version ([8b551e9](https://gitlab.com/lmco/robots/semantic-release-image/commit/8b551e921b95006ceb5dbb105fd33f52662c6515))
* Update semantic release versions ([f39c2f9](https://gitlab.com/lmco/robots/semantic-release-image/commit/f39c2f96720afd00822dc7e7d24f98b2471daa51))
* Update signing module ([a5789bb](https://gitlab.com/lmco/robots/semantic-release-image/commit/a5789bb76e9175705f3403dbaf35745d70433106))

## [0.0.63](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.62...v0.0.63) (2023-08-27)


### Bug Fixes

* **deps:** update dependency @semantic-release/commit-analyzer to v10.0.2 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([f13fb95](https://gitlab.com/lmco/robots/semantic-release-image/commit/f13fb9576a5ed04391c235f9a962df94e2eb3b7b))

## [0.0.62](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.61...v0.0.62) (2023-08-27)


### Bug Fixes

* **deps:** update dependency @semantic-release/release-notes-generator to v11.0.5 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([dea6c59](https://gitlab.com/lmco/robots/semantic-release-image/commit/dea6c59398776c38bdf7d1420c81683ff681e799))

## [0.0.61](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.60...v0.0.61) (2023-08-19)


### Bug Fixes

* **deps:** update dependency @semantic-release/npm to v10.0.5 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([fc3dd62](https://gitlab.com/lmco/robots/semantic-release-image/commit/fc3dd62b570e8e4f21cd1876ee178b1f037012d5))

## [0.0.60](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.59...v0.0.60) (2023-08-09)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v12.0.5 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([fa93b7d](https://gitlab.com/lmco/robots/semantic-release-image/commit/fa93b7d8af6062d373b66fe28c7ec8f114d88eab))

## [0.0.59](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.58...v0.0.59) (2023-08-03)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v12.0.4 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([41afcbb](https://gitlab.com/lmco/robots/semantic-release-image/commit/41afcbb55c18f11b327282d94e8ba6aed0803c5e))

## [0.0.58](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.57...v0.0.58) (2023-07-24)


### Bug Fixes

* Add helm3 module ([c9347f3](https://gitlab.com/lmco/robots/semantic-release-image/commit/c9347f345bcc97adf7d7ddf318a094645f616d3a))

## [0.0.57](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.56...v0.0.57) (2023-07-21)


### Bug Fixes

* **deps:** update dependency @google/semantic-release-replace-plugin to v1.2.7 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([e70ed8f](https://gitlab.com/lmco/robots/semantic-release-image/commit/e70ed8ffd1a673d6d2f2f1e8221ea76458040310))

## [0.0.56](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.55...v0.0.56) (2023-07-13)


### Bug Fixes

* Add jdk maven and gradle ([1b597c1](https://gitlab.com/lmco/robots/semantic-release-image/commit/1b597c1b960f1dec88d92c7e8ecde3ffb29ca535))

## [0.0.55](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.54...v0.0.55) (2023-07-07)


### Bug Fixes

* **deps:** update dependency @google/semantic-release-replace-plugin to v1.2.5 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([9398044](https://gitlab.com/lmco/robots/semantic-release-image/commit/939804482b1c81a0e7393287c70def613f3e1dae))

## [0.0.54](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.53...v0.0.54) (2023-07-06)


### Bug Fixes

* **deps:** update dependency @google/semantic-release-replace-plugin to v1.2.4 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([5934cdd](https://gitlab.com/lmco/robots/semantic-release-image/commit/5934cddac57a9c4b84fd7c1112576cffe6ae6c8c))

## [0.0.53](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.52...v0.0.53) (2023-07-06)


### Bug Fixes

* **deps:** update dependency @google/semantic-release-replace-plugin to v1.2.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([ba92804](https://gitlab.com/lmco/robots/semantic-release-image/commit/ba9280435b44b678664cf044dbbd76b3935282a3))

## [0.0.52](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.51...v0.0.52) (2023-07-05)


### Bug Fixes

* Add major-bump fail plugin ([e7c4d73](https://gitlab.com/lmco/robots/semantic-release-image/commit/e7c4d732128375da64d36adc32d35dd80835066a))
* Setup bash as default environment ([eb74bc4](https://gitlab.com/lmco/robots/semantic-release-image/commit/eb74bc411def2b66a981bec4aabc6d4c2ef942af))

## [0.0.52](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.51...v0.0.52) (2023-07-05)


### Bug Fixes

* Setup bash as default environment ([eb74bc4](https://gitlab.com/lmco/robots/semantic-release-image/commit/eb74bc411def2b66a981bec4aabc6d4c2ef942af))

## [0.0.51](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.50...v0.0.51) (2023-07-04)


### Bug Fixes

* **deps:** update dependency @semantic-release/release-notes-generator to v11.0.4 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([7328c05](https://gitlab.com/lmco/robots/semantic-release-image/commit/7328c05b7e9fab25ef4a9f5bdd0e6d52baba8d12))

## [0.0.50](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.49...v0.0.50) (2023-06-30)


### Bug Fixes

* Update semantic-release image ([5f5cf48](https://gitlab.com/lmco/robots/semantic-release-image/commit/5f5cf4829e0b3e7304213680f5d9ea8ac86bae79))

## [0.0.49](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.48...v0.0.49) (2023-06-29)


### Bug Fixes

* Downgrade release version ([85a56c7](https://gitlab.com/lmco/robots/semantic-release-image/commit/85a56c726b21c412b97607b9cd25599be0ed159b))

## [0.0.48](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.47...v0.0.48) (2023-06-29)


### Bug Fixes

* bump version ([eaca40d](https://gitlab.com/lmco/robots/semantic-release-image/commit/eaca40d9d888dafe81cfd9d690f3d2fa0814f5da))

## [0.0.47](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.46...v0.0.47) (2023-06-28)


### Bug Fixes

* **deps:** update dependency semantic-release to v21.0.6 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([a830fc8](https://gitlab.com/lmco/robots/semantic-release-image/commit/a830fc82f5bce2c65db57f13e0a91989540d8dec))

## [0.0.46](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.45...v0.0.46) (2023-06-17)


### Bug Fixes

* **deps:** update dependency conventional-changelog-conventionalcommits to v6.1.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([0b7f1b4](https://gitlab.com/lmco/robots/semantic-release-image/commit/0b7f1b4192e7043ca1f2d6e1bc8bffeceb379155))

## [0.0.45](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.44...v0.0.45) (2023-06-14)


### Bug Fixes

* Add cosign to image ([1e3ed81](https://gitlab.com/lmco/robots/semantic-release-image/commit/1e3ed81e1921e458b240496a12e58d20227face4))

## [0.0.44](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.43...v0.0.44) (2023-06-12)


### Bug Fixes

* Add goreleaser ([52bc653](https://gitlab.com/lmco/robots/semantic-release-image/commit/52bc6531954f703c7720c815ee71dd14e9cb58ed))
* Don't add maven ([120ac3f](https://gitlab.com/lmco/robots/semantic-release-image/commit/120ac3fdc892be00c9b368ab686716af739754d8))

## [0.0.43](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.42...v0.0.43) (2023-06-10)


### Bug Fixes

* **deps:** update dependency semantic-release to v21.0.5 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([9fc37a7](https://gitlab.com/lmco/robots/semantic-release-image/commit/9fc37a74d1cff6ff5438cda99db113bcb960f787))

## [0.0.42](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.41...v0.0.42) (2023-06-09)


### Bug Fixes

* **deps:** update semantic-release monorepo signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([00d4023](https://gitlab.com/lmco/robots/semantic-release-image/commit/00d40234feda59b3a796da26b4a4283c6f7570af))

## [0.0.41](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.40...v0.0.41) (2023-06-07)


### Bug Fixes

* **deps:** update semantic-release monorepo signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([6663607](https://gitlab.com/lmco/robots/semantic-release-image/commit/6663607f6bcc12603e393d73a101bc0e8c43e7c0))

## [0.0.40](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.39...v0.0.40) (2023-06-06)


### Bug Fixes

* **deps:** update dependency conventional-changelog-conventionalcommits to v6 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([86a69d7](https://gitlab.com/lmco/robots/semantic-release-image/commit/86a69d74a62cbd2d6cb7bd57a1997361257eb8b1))

## [0.0.39](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.38...v0.0.39) (2023-06-03)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v12.0.2 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([e566183](https://gitlab.com/lmco/robots/semantic-release-image/commit/e566183aa946c2c02993a6cc5a5b6f24dd14318f))

## [0.0.38](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.37...v0.0.38) (2023-06-02)


### Bug Fixes

* **deps:** update dependency semantic-release to v21.0.3 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([28fbc6d](https://gitlab.com/lmco/robots/semantic-release-image/commit/28fbc6d3560608d74c630d86f31ceee7330a46a9))

## [0.0.37](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.36...v0.0.37) (2023-06-02)


### Bug Fixes

* **deps:** update dependency @semantic-release/commit-analyzer to v10 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([dc8f42a](https://gitlab.com/lmco/robots/semantic-release-image/commit/dc8f42ad6174009e8765d00876b8eb7d6e70ab58))

## [0.0.36](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.35...v0.0.36) (2023-05-28)


### Bug Fixes

* **deps:** update dependency @semantic-release/release-notes-generator to v11.0.2 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([44b7358](https://gitlab.com/lmco/robots/semantic-release-image/commit/44b7358e3252676600283ff1f3505ba75ed47bb2))

## [0.0.35](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.34...v0.0.35) (2023-05-22)


### Bug Fixes

* **deps:** update dependency semantic-release-slack-bot to v4.0.2 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([a73b58b](https://gitlab.com/lmco/robots/semantic-release-image/commit/a73b58b0bb9fb1d7553452841084bd96e24eaf21))

## [0.0.34](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.33...v0.0.34) (2023-05-18)


### Bug Fixes

* Add backmerge ([408a72c](https://gitlab.com/lmco/robots/semantic-release-image/commit/408a72c87f35d629162c4d34a71ce70419dd13cd))

## [0.0.33](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.32...v0.0.33) (2023-05-05)


### Bug Fixes

* **deps:** update dependency @semantic-release/release-notes-generator to v11 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([4d77de8](https://gitlab.com/lmco/robots/semantic-release-image/commit/4d77de8167b23031d87c6a15feb3b26fd88bf50f))
* **deps:** update dependency semantic-release to v21.0.2 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([8707dd7](https://gitlab.com/lmco/robots/semantic-release-image/commit/8707dd7b6114c2ab2bb88542c41115fd642580d2))

## [0.0.32](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.31...v0.0.32) (2023-04-24)


### Bug Fixes

* add openssh to access git per SSH ([f2141ab](https://gitlab.com/lmco/robots/semantic-release-image/commit/f2141abeaf714dc6b1f7dec34e448f664823c9b7))

## [0.0.31](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.30...v0.0.31) (2023-04-07)


### Bug Fixes

* **deps:** update dependency semantic-release-slack-bot to v4.0.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([9b1025f](https://gitlab.com/lmco/robots/semantic-release-image/commit/9b1025f8451ab094a92f52d28a5c28b36da10645))

## [0.0.30](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.29...v0.0.30) (2023-04-07)


### Bug Fixes

* **deps:** update dependency @semantic-release/npm to v10.0.3 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([5504f04](https://gitlab.com/lmco/robots/semantic-release-image/commit/5504f04456863b7b03f3bcb3a054a147d4ac96fa))

## [0.0.29](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.28...v0.0.29) (2023-04-01)


### Bug Fixes

* **deps:** update dependency semantic-release to v21.0.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([d5d32ca](https://gitlab.com/lmco/robots/semantic-release-image/commit/d5d32caa8839fa7b46fd9061efc39eaa6cb2c0dc))

## [0.0.28](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.27...v0.0.28) (2023-03-25)


### Bug Fixes

* **deps:** update dependency semantic-release to v21 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([8693c5a](https://gitlab.com/lmco/robots/semantic-release-image/commit/8693c5aadeb2ae9395b2a2f34808bdf53cbc7778))

## [0.0.27](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.26...v0.0.27) (2023-03-23)


### Bug Fixes

* **deps:** update dependency @semantic-release/changelog to v6.0.3 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([72465de](https://gitlab.com/lmco/robots/semantic-release-image/commit/72465dea1b518b25d3f311815988f16a8e47ae08))

## [0.0.26](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.25...v0.0.26) (2023-03-23)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v12.0.1 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([12f2c1e](https://gitlab.com/lmco/robots/semantic-release-image/commit/12f2c1e236cf814b7c642f0fff85acf437afa805))

## [0.0.25](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.24...v0.0.25) (2023-03-22)


### Bug Fixes

* **deps:** update dependency @semantic-release/npm to v10 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([18ba2f0](https://gitlab.com/lmco/robots/semantic-release-image/commit/18ba2f0ddabf6a818bc78151c5fd5a0ecb80ddb8))

## [0.0.24](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.23...v0.0.24) (2023-03-17)


### Bug Fixes

* **deps:** update dependency semantic-release to v20.1.3 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([e1f3a35](https://gitlab.com/lmco/robots/semantic-release-image/commit/e1f3a35d701de1faecc53595f8e49ed61addc948))

## [0.0.23](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.22...v0.0.23) (2023-03-17)


### Bug Fixes

* **deps:** update dependency semantic-release to v20.1.2 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([040782c](https://gitlab.com/lmco/robots/semantic-release-image/commit/040782c2153ebff20a30d8d0f1f799c7bd658fbc))

## [0.0.22](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.21...v0.0.22) (2023-03-14)


### Bug Fixes

* **deps:** update dependency semantic-release-slack-bot to v4 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([ad6a7d3](https://gitlab.com/lmco/robots/semantic-release-image/commit/ad6a7d37f1634fa1d1b3913f90bdbd1f30d9af30))
* **deps:** update semantic-release monorepo signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([33c23b1](https://gitlab.com/lmco/robots/semantic-release-image/commit/33c23b1e8de0d778f3c0563af7a68b4c2fae3e47))

## [0.0.21](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.20...v0.0.21) (2023-03-13)


### Bug Fixes

* **deps:** update dependency @google/semantic-release-replace-plugin to v1.2.0 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([3edd5dd](https://gitlab.com/lmco/robots/semantic-release-image/commit/3edd5dd67d37a3f5e04176d150cd19a8875e91f0))

## [0.0.20](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.19...v0.0.20) (2023-03-13)


### Bug Fixes

* **deps:** update dependency @semantic-release/npm to v9.0.2 signed-off-by: bot, renovate <hopprexternalrenovate.dl-eo@groups.lmco.com> ([2c1fa3b](https://gitlab.com/lmco/robots/semantic-release-image/commit/2c1fa3b08cb8d9ae1930419cbcc9d799a8cdce7c))

## [0.0.19](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.18...v0.0.19) (2023-02-18)


### Bug Fixes

* Patch additional core dependencies ([d3dc6e9](https://gitlab.com/lmco/robots/semantic-release-image/commit/d3dc6e98bf9084d5981f14def96be8871c9c6a5a))

## [0.0.18](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.17...v0.0.18) (2023-01-11)


### Bug Fixes

* Add git-lfs ([43fe299](https://gitlab.com/lmco/robots/semantic-release-image/commit/43fe2996c18a38e1fc45be90e4a1acf8872639d8))

## [0.0.17](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.16...v0.0.17) (2022-12-08)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v9.5.1 ([254df88](https://gitlab.com/lmco/robots/semantic-release-image/commit/254df8830c80625068504928707bddea8e4556e1))

## [0.0.16](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.15...v0.0.16) (2022-11-29)


### Bug Fixes

* **deps:** update dependency @semantic-release/changelog to v6.0.2 ([25043e6](https://gitlab.com/lmco/robots/semantic-release-image/commit/25043e6b1d7c86f76d5bcf6f1119501ce3e27b7d))

## [0.0.15](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.14...v0.0.15) (2022-11-19)


### Bug Fixes

* Add python and poetry ([72185ab](https://gitlab.com/lmco/robots/semantic-release-image/commit/72185ab5e8a08dad4ad6aaa407cb2651024b2263))

## [0.0.14](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.13...v0.0.14) (2022-11-19)


### Bug Fixes

* Add semantic-release-python ([86ed2d5](https://gitlab.com/lmco/robots/semantic-release-image/commit/86ed2d52038c371eb2e370ee747f5e504d74671a))

## [0.0.13](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.12...v0.0.13) (2022-11-13)


### Bug Fixes

* Correct release artifact ([ae71c1e](https://gitlab.com/lmco/robots/semantic-release-image/commit/ae71c1e730e432a482673f77fa1f14346fedf303))

## [0.0.12](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.11...v0.0.12) (2022-11-13)


### Bug Fixes

* Cosign.pub name matters ([526c834](https://gitlab.com/lmco/robots/semantic-release-image/commit/526c834774adf0b69d9f027a167bffe20014722c))
* Remove gitlab.pub key ([e2b19fa](https://gitlab.com/lmco/robots/semantic-release-image/commit/e2b19faaed8f1fd611febd236ca122ef3ac63419))
* Use the digest in the bom name ([9417eb1](https://gitlab.com/lmco/robots/semantic-release-image/commit/9417eb193b34d047b36528defb562971ee739fa3))
* Verify signatures ([02016f7](https://gitlab.com/lmco/robots/semantic-release-image/commit/02016f70ca9ecb5a6a97b0268412121a8b5962ac))

## [0.0.11](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.10...v0.0.11) (2022-11-13)


### Bug Fixes

* Add build dependencies ([9385790](https://gitlab.com/lmco/robots/semantic-release-image/commit/9385790154fb65fe1ae9659f4b684e0509c3cd04))
* Change signing image ([502212e](https://gitlab.com/lmco/robots/semantic-release-image/commit/502212ee9d73ae71ba5808625e2e5dd85ac0e341))
* Change signing image ([500d151](https://gitlab.com/lmco/robots/semantic-release-image/commit/500d15198e1e93c23c2efe192505ab74e5eaa140))
* Cleanup apt install ([a1f5432](https://gitlab.com/lmco/robots/semantic-release-image/commit/a1f5432e4b520f1da260a602ce983867e9b3e9bc))
* Sign with hidden file ([a4fd8c6](https://gitlab.com/lmco/robots/semantic-release-image/commit/a4fd8c6db26f65a110d051f6809a6de1b5d06fd9))
* Sign with hidden file and bash ([c2fc376](https://gitlab.com/lmco/robots/semantic-release-image/commit/c2fc3766e577740af3dd6c8ed7cc9cf5a2c2445a))

## [0.0.10](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.9...v0.0.10) (2022-11-12)


### Bug Fixes

* Cleanup automation ([dd42dfd](https://gitlab.com/lmco/robots/semantic-release-image/commit/dd42dfdd7044e586318f0aa81dbe540a09b7b927))

## [0.0.9](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.8...v0.0.9) (2022-11-12)


### Bug Fixes

* Add dockerhub push ([861f2e6](https://gitlab.com/lmco/robots/semantic-release-image/commit/861f2e65ef82a44d15607c683f55a00703774088))
* Correct the docker endpoint ([e9d887b](https://gitlab.com/lmco/robots/semantic-release-image/commit/e9d887bfa3aa7d8add976b098601f50cfd245f43))
* Sign everything ([b87500c](https://gitlab.com/lmco/robots/semantic-release-image/commit/b87500cce656203d213241bd37c3a30ebe9ec2a1))
* Sign everything ([b640771](https://gitlab.com/lmco/robots/semantic-release-image/commit/b640771887f9daa05086bb6ec41831e92cad5c23))
* Sign everything ([dbd20ed](https://gitlab.com/lmco/robots/semantic-release-image/commit/dbd20ed659e823ca28fdbf2c6b70835bfbb5ce4f))
* Sign everything ([33f078c](https://gitlab.com/lmco/robots/semantic-release-image/commit/33f078ccbb6b3c93a56b20a601da8555b42dfbb1))

## [0.0.8](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.7...v0.0.8) (2022-11-11)


### Bug Fixes

* **deps:** update dependency semantic-release-helm to v2.2.0 ([a2929bc](https://gitlab.com/lmco/robots/semantic-release-image/commit/a2929bc3724bc7f7ba74e3413f176ce08bb44d21))

## [0.0.7](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.6...v0.0.7) (2022-11-11)


### Bug Fixes

* Correct readme ([3c0bb2f](https://gitlab.com/lmco/robots/semantic-release-image/commit/3c0bb2f9f33e06fcd5bb8a4af0f1f6462c23fac8))
* Correct semantic release command ([82bc239](https://gitlab.com/lmco/robots/semantic-release-image/commit/82bc239d00563865f02c4ce31f9105ac51d469c0))
* Identify artifact attestation ([6783bda](https://gitlab.com/lmco/robots/semantic-release-image/commit/6783bda109fc5da362d0d918bdf80511e6760ba7))
* Identify artifact attestation ([ba31256](https://gitlab.com/lmco/robots/semantic-release-image/commit/ba3125628456745ba3e2b708e7f7fcf25388cefc))
* Identify artifact attestation ([047d381](https://gitlab.com/lmco/robots/semantic-release-image/commit/047d3812c9b38f897c740b5d0809bfdc3615e19b))
* Identify artifact attestation ([eb1727b](https://gitlab.com/lmco/robots/semantic-release-image/commit/eb1727b6ee01f8a8e08415229e3574e246081aad))

## [0.0.6](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.5...v0.0.6) (2022-11-11)


### Bug Fixes

* Add readme ([da64a02](https://gitlab.com/lmco/robots/semantic-release-image/commit/da64a0237d7a3a77c7c4035e3c2670d58bce07f6))

## [0.0.5](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.4...v0.0.5) (2022-11-11)


### Bug Fixes

* Add readme ([6ae781c](https://gitlab.com/lmco/robots/semantic-release-image/commit/6ae781cabbacf9ae429888723b09dc17fb86ac3b))
* Correct git repository ([4858c4a](https://gitlab.com/lmco/robots/semantic-release-image/commit/4858c4a6962ea6ad71d72c6878da3c0af2f31b59))
* Correct git repository ([bf0740e](https://gitlab.com/lmco/robots/semantic-release-image/commit/bf0740e300e8e2a34060dc0e254a4117e14ecc34))
* Correct git repository ([949cf6a](https://gitlab.com/lmco/robots/semantic-release-image/commit/949cf6abd6be3c59e94d9a736082bf584448f6da))
* Correct git repository ([d322e77](https://gitlab.com/lmco/robots/semantic-release-image/commit/d322e7700039c86639d9b080c1114e3f3cbbf4c9))
* Verify signature ([c799bac](https://gitlab.com/lmco/robots/semantic-release-image/commit/c799bac94c41eea79e6b2669ef4027b514508774))

## [0.0.4](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.3...v0.0.4) (2022-11-11)


### Bug Fixes

* **deps:** update dependency @semantic-release/gitlab to v9.5.0 ([7243bc1](https://gitlab.com/lmco/robots/semantic-release-image/commit/7243bc1eed1e7f82336a6297d792d9393a989799))

## [0.0.3](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.2...v0.0.3) (2022-11-11)


### Bug Fixes

* Use public rekor ([8cde505](https://gitlab.com/lmco/robots/semantic-release-image/commit/8cde5052efa6030a4a7d214cd2812a6baebe0340))

## [0.0.2](https://gitlab.com/lmco/robots/semantic-release-image/compare/v0.0.1...v0.0.2) (2022-11-11)


### Bug Fixes

* Reset versions and attach pub key ([dae1573](https://gitlab.com/lmco/robots/semantic-release-image/commit/dae15737f7a16095976e458aa6320529b9ccd84a))
