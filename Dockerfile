FROM node:lts-alpine3.20@sha256:d3bec89af3388e8a0842860fc2a6de688e3841d06a69453b552ce0b9e6be589e

COPY package.json /opt/app/package.json

WORKDIR /opt/app

RUN apk add --no-cache bash openssl openjdk17 maven gradle python3 py3-pip python3-dev git openssh curl jq yq git-lfs curl ca-certificates step-cli unzip go cosign && \
    pip install --ignore-installed --break-system-packages poetry && \
    yarn install --immutable --immutable-cache --check-cache && \
    curl -sL https://github.com/goreleaser/goreleaser/releases/download/v1.18.2/goreleaser_1.18.2_x86_64.apk -o goreleaser.apk && \
    apk add --allow-untrusted goreleaser.apk && \
    curl https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash && \
    helm plugin install https://github.com/chartmuseum/helm-push && \
    ln -sf /bin/bash /bin/sh

ENV PATH=$PATH:/root/node_modules/.bin:/opt/app/node_modules/.bin
